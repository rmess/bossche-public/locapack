module.exports = function(RED) {
    function encodeUniversalGnssPacketNode(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {

            const buff = Buffer.alloc(128);
            var len = 0;
            var valid_gnss_position = false;
            var altitude_presence_flag = false;
            var dop_presence_flag = false;

            // Check optional fields
            if ( (typeof msg.payload.latitude !== 'undefined') && (typeof msg.payload.longitude !== 'undefined') ) valid_gnss_position = true;
            if (typeof msg.payload.altitude !== 'undefined') altitude_presence_flag = true;
            if (typeof msg.payload.dop !== 'undefined') dop_presence_flag = true;

            // Make header
            if (altitude_presence_flag) buff[len] |= 0x80;
            if (dop_presence_flag) buff[len] |= 0x40;
            if (valid_gnss_position) buff[len] |= 0x20;
            len += 1;

            // Make latitude and longitude fields
            if ( valid_gnss_position ) {
                buff.writeFloatLE(msg.payload.latitude, len);
                len += 4;
                buff.writeFloatLE(msg.payload.longitude, len);
                len += 4;
            }

            // Make altitude field if present
            if ( altitude_presence_flag ) {
                buff.writeFloatLE(msg.payload.altitude, len);
                len += 4;
            }

            // Make dop field if present
            if ( dop_presence_flag ) {
                buff.writeFloatLE(msg.payload.dop, len);
                len += 4;
            }

            msg.payload["LocaPack"] = {};
            msg.payload["LocaPack"]["universalGnssPacket"] = {};
            msg.payload["LocaPack"]["universalGnssPacket"]["buffer"] = buff.subarray(0, len);
            node.send(msg);
        });
    }
    RED.nodes.registerType("encode GNSS",encodeUniversalGnssPacketNode);
}
