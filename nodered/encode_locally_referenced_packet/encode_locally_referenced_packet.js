module.exports = function(RED) {
    function encodeLocallyReferencedPacketNode(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {

            const buff = Buffer.alloc(128);
            var len = 0;
            var z_presence_flag = false;
            var dop_presence_flag = false;
            var frameofref_id_presence_flag = false;

            // Check mandatory fields
            if (typeof msg.payload.x === 'undefined') return;
            if (typeof msg.payload.y === 'undefined') return;

            // Check optional fields
            if (typeof msg.payload.z !== 'undefined') z_presence_flag = true;
            if (typeof msg.payload.dop !== 'undefined') dop_presence_flag = true;
            if (typeof msg.payload.frameofref_id !== 'undefined') frameofref_id_presence_flag = true;

            // Make header
            if (z_presence_flag) buff[len] |= 0x80;
            if (dop_presence_flag) buff[len] |= 0x40;
            if (frameofref_id_presence_flag) buff[len] |= 0x20;
            len += 1;

            // Make x and y fields
            buff.writeFloatLE(msg.payload.x, len);
            len += 4;
            buff.writeFloatLE(msg.payload.y, len);
            len += 4;

            // Make z field if present
            if ( z_presence_flag ) {
                buff.writeFloatLE(msg.payload.z, len);
                len += 4;
            }

            // Make dop field if present
            if ( dop_presence_flag ) {
                buff.writeFloatLE(msg.payload.dop, len);
                len += 4;
            }

            // Make frameofref_id field if present
            if ( frameofref_id_presence_flag ) {
                buff.writeUInt32LE(msg.payload.frameofref_id%Math.pow(2, 32), len); // 32-bit LSB
                len += 4;
                buff.writeUInt32LE(msg.payload.frameofref_id/Math.pow(2, 32), len); // 32-bit MSB
                len += 4;
            }

            msg.payload["LocaPack"] = {};
            msg.payload["LocaPack"]["locallyReferencedPacket"] = {};
            msg.payload["LocaPack"]["locallyReferencedPacket"]["buffer"] = buff.subarray(0, len);
            node.send(msg);
        });
    }
    RED.nodes.registerType("encode LocRef",encodeLocallyReferencedPacketNode);
}
