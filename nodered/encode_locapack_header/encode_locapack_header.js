module.exports = function(RED) {
    function encodeLocaPackHeaderNode(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {

            const buff = Buffer.alloc(128);
            var len = 0;
            var header = 0;
            var timestamp_presence_flag = false;
            var movement_id_presence_flag = false;
            var packet_type;

            // Check mandatory fields
            if (typeof msg.payload.sequence_number === 'undefined') return;

            // Check optional fields
            if (typeof msg.payload.timestamp !== 'undefined') timestamp_presence_flag = true;
            if (typeof msg.payload.movement_id !== 'undefined') movement_id_presence_flag = true;
            if (typeof msg.payload.device_id !== 'undefined') {
                // device_id_size is mandatory if a device_id is given
                if (typeof msg.payload.device_id_size === 'undefined') return;
                device_id_presence_flag = true;
            }

            // Determine packet_type or exit
            if (typeof msg.payload.LocaPack.universalGnssPacket !== 'undefined') packet_type = 1;
            if (typeof msg.payload.LocaPack.locallyReferencedPacket !== 'undefined') packet_type = 2;
            if (packet_type === 'undefined') return;

            // Make header
            header |= 1 << 12; // Protocol version 1
            header |= msg.payload.device_id_size << 8;
            header |= packet_type << 4;
            if (movement_id_presence_flag) header |= 0x0008;
            if (device_id_presence_flag) header |= 0x0004;
            buff.writeUInt16LE(header, len);
            len += 2;

            // Make sequence number field
            buff.writeUInt16LE(msg.payload.sequence_number, len);
            len += 2;

            // Make timestamp field if present (5 bytes)
            if ( timestamp_presence_flag ) {
                buff.writeUIntLE(msg.payload.timestamp, len, 5);
                len += 5;
            }

            // Make movement_id field if present
            if ( movement_id_presence_flag ) {
                buff[len] = msg.payload.movement_id;
                len += 1;
            }

            // Make device_id field if present
            if ( device_id_presence_flag ) {
                buff.writeUIntLE(msg.payload.device_id, len, msg.payload.device_id_size);
                len += msg.payload.device_id_size;
            }

            // Add payload len and payload
            if ( packet_type == 1 ) {
                payload_len = msg.payload.LocaPack.universalGnssPacket.buffer.length;
                buff[len] = payload_len
                len += 1;
                var header_buff_universalGnssPacket = buff.subarray(0, len);
                msg.payload["LocaPack"]["outputBuffer"] = Buffer.concat([header_buff_universalGnssPacket, msg.payload.LocaPack.universalGnssPacket.buffer], len + payload_len);
            } else if ( packet_type == 2 ) {
                payload_len = msg.payload.LocaPack.locallyReferencedPacket.buffer.length;
                buff[len] = payload_len
                len += 1;
                var header_buff_locallyReferencedPacket = buff.subarray(0, len);
                msg.payload["LocaPack"]["outputBuffer"] = Buffer.concat([header_buff_locallyReferencedPacket, msg.payload.LocaPack.locallyReferencedPacket.buffer], len + payload_len);
            } else return;

            node.send(msg);
        });
    }
    RED.nodes.registerType("encode header",encodeLocaPackHeaderNode);
}
