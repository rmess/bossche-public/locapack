module.exports = function(RED) {
    function decodeLocaPackHeaderNode(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {

            var p = {};
            var buff = msg.payload.LocaPack.inputBuffer;
            var len = 0;
            
            // Get header Field (hdr)
            var hdr = {};
            var hdr_raw = buff.readUInt16LE(len);
            hdr.protocol_version = (hdr_raw & 0xF000) >> 12 ;
            hdr.device_id_size = (hdr_raw & 0x0F00) >> 8;
            hdr.packet_type = (hdr_raw & 0x00F0) >> 4;
            hdr.movement_id_presence_flag = (hdr_raw & 0x0008) ? true : false;
            hdr.timestamp_presence_flag = (hdr_raw & 0x0004) ? true : false;
            p.header = hdr;
            len += 2;
            
            // Sequence number
            p.sequence_number = buff.readUInt16LE(len);
            len += 2;
            
            // If the message contains the timestamp field (40 bits)
            if (p.header.timestamp_presence_flag ) {
                p.timestamp = buff.readUIntLE(len, 5);
                len += 5;
            }
            
            // If the message contains the movement_id field (8 bits)
            if (p.header.movement_id_presence_flag ) {
                p.movement_id = parseInt(buff[len]);
                len += 1;
            }
            
            // If the message contains the device_id field (n bytes)
            if (p.header.device_id_size > 0 ) {
                var n = p.header.device_id_size;
                p.device_id = buff.readUIntLE(len, n);
                len += n;
            }
            
            // Payload_len field (8 bits)
            var payload_len = parseInt(buff[len]);
            len += 1;
            
            // payload
            p.payload = buff.subarray(len, len+payload_len)
            
            msg.payload["LocaPack"] = p;
            
            node.send(msg);
        });
    }
    RED.nodes.registerType("decode header",decodeLocaPackHeaderNode);
}
