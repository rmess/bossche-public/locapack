# locapack

## Brief

Another localisation packet protocol to send/receive localisation data over networks.

## Details

LocaPack is an implementation of the locapack protocol used to send or receive various localisation data over wired and wireless networks.

LocaPack enables the transport of:
- universal GNSS localisation coordonates such as latitude/longitude data, 
- locally referenced localisations, such as {x,y} positions in an indoor environment,
- inter-node proximity information such as ranging data.

Some extra features are also avalable, such as:
- automatic mapping/translation of coordonates between universal GNSS/locally referenced localisations,
- movement_id, that enables extra movement information based on another sensor (typically a local accelerometer),
- packet sequence numbering and local timestamping, to track positions and detect missing information,
- message hashing, to send shorter but incomplete payloads, to respect privacy.
 
Several implementations are available: 
- Embedded: C++ class (Arduino library)
- Infrastrucure/IoT: Node-RED palette
- Portable: python

A Json description of the messages is also available.

## Implementation

LocaPack is available as a Node-RED Palette, ie a collection of nodes to produce or decode locapack messages.

![Flow Example](node-red-flow-example.png)

Node-RED implementation status:

- encode/decode header,
- encode/decode universal GNSS packets,
- encode/decode locally referenced packets.

### JSON messages

LocaPack messages are also available in JSON. 

Example of a Universal GNSS localisation packet:
```json
{
  "header": {
    "protocol_version": 1,
    "device_id_size": 2,
    "packet_type": 1,
    "movement_id_presence_flag": false,
    "timestamp_presence_flag": true
  },
  "sequence_number": 2746,
  "timestamp": 27850001,
  "device_id": 237,
  "payload": [192,49,131,47,66,150,8,165,63,154,153,18,67,164,112,157,63],
  "universalGnssPacket": {
    "header": {
      "altitude_presence_flag": true,
      "dop_presence_flag": true
    },
    "latitude": 43.9,
    "longitude": 1.1,
    "altitude": 146.6,
    "dop": 1.2300000190734863
  }
}
```

Example of a Locally Referenced localisation packet:
```json
{
  "LocaPack": {
    "header": {
      "protocol_version": 1,
      "device_id_size": 2,
      "packet_type": 2,
      "movement_id_presence_flag": false,
      "timestamp_presence_flag": true
    },
    "sequence_number": 163,
    "timestamp": 1630289,
    "device_id": 124,
    "payload": [224,0,0,128,63,0,0,0,64,0,0,64,64,0,0,128,64,136,119,102,85,68,51,34,17],
    "locallyReferencedPacket": {
      "header": {
        "z_presence_flag": true,
        "dop_presence_flag": true,
        "frameofref_id_presence_flag": true
      },
      "x": 1,
      "y": 2,
      "z": 3,
      "dop": 4,
      "frameofref_id": 9833440827789222000
    }
  }
}
```

## License

Locapack is distributed under the GPLv3 license. 

