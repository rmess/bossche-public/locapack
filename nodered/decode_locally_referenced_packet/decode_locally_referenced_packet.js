module.exports = function(RED) {
    function decodeLocallyReferencedPacketNode(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {

            var p = {};
            var buff = msg.payload.LocaPack.payload;
            var len = 0;
            
            // Get Header Field (hdr)
            var hdr = {};
            var hdr_raw = parseInt(buff[len]);
            hdr.z_presence_flag = (hdr_raw & 0x80) ? true : false;
            hdr.dop_presence_flag = (hdr_raw & 0x40) ? true : false;
            hdr.frameofref_id_presence_flag = (hdr_raw & 0x20) ? true : false;
            p.header = hdr;
            len += 1;
            
            // Latitude
            p.x = buff.readFloatLE(len);
            len += 4;
            
            // Longitude
            p.y = buff.readFloatLE(len);
            len += 4;
            
            // If the message contains the z field
            if (p.header.z_presence_flag ) {
                p.z = buff.readFloatLE(len);
                len += 4;
            }
            
            // If the message contains the dop field
            if (p.header.dop_presence_flag ) {
                p.dop = buff.readFloatLE(len);
                len += 4;
            }

            // If the message contains the frameofref_id field
            if (p.header.frameofref_id_presence_flag ) {
                var lsB =  buff.readUInt32LE(len); // 32-bit LSB
                len += 4;
                var msB =  buff.readUInt32LE(len); // 32-bit MSB
                len += 4;
                p.frameofref_id = lsB + msB * Math.pow(2, 32);
            }
            
            msg.payload.LocaPack["locallyReferencedPacket"] = p;

            node.send(msg);
        });
    }
    RED.nodes.registerType("decode LocRef",decodeLocallyReferencedPacketNode);
}
