module.exports = function(RED) {
    function decodeUniversalGnssPacketNode(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {

            var p = {};
            var buff = msg.payload.LocaPack.payload;
            var len = 0;
            
            // Get Header Field (hdr)
            var hdr = {};
            var hdr_raw = parseInt(buff[len]);
            hdr.altitude_presence_flag = (hdr_raw & 0x80) ? true : false;
            hdr.dop_presence_flag = (hdr_raw & 0x40) ? true : false;
            hdr.valid_gnss_position = (hdr_raw & 0x20) ? true : false;
            p.header = hdr;
            len += 1;
            
            // If the message has a valid_gnss_position
            if (p.header.valid_gnss_position ) {
                p.latitude = buff.readFloatLE(len);
                len += 4;
                p.longitude = buff.readFloatLE(len);
                len += 4;
            }

            // If the message contains the altitude field
            if (p.header.altitude_presence_flag ) {
                p.altitude = buff.readFloatLE(len);
                len += 4;
            }
            
            // If the message contains the dop field
            if (p.header.dop_presence_flag ) {
                p.dop = buff.readFloatLE(len);
                len += 4;
            }
            
            msg.payload.LocaPack["universalGnssPacket"] = p;

            node.send(msg);
        });
    }
    RED.nodes.registerType("decode GNSS",decodeUniversalGnssPacketNode);
}
