// Locapack.cpp
// 
// Locapack is an Arduino library that implements the localisation packet protocol from the IRIT/RMESS team. 

#include <SPI.h>
#include "Locapack.h"
#include <machine/endian.h>


Locapack::Locapack()
{
	_sequence_number_universalGnssPacket = 0;
	_sequence_number_locallyReferencedPacket = 0;
	setdevice_id16(0);
}


void Locapack::setdevice_id16(uint16_t device_id)
{
	encodeUint16(device_id, _device_id.device_id_buf);
	_device_id.device_id_len = 2;	
}


void Locapack::setdevice_id32(uint32_t device_id)
{
	encodeUint32(device_id, _device_id.device_id_buf);
	_device_id.device_id_len = 4;	
}


void Locapack::setdevice_id64(uint64_t device_id)
{
	encodeUint64(device_id, _device_id.device_id_buf);
	_device_id.device_id_len = 8;	
}


int Locapack::makeHeader(protocol_version_t protocol_version, uint8_t movement_id_presence_flag, 
	uint8_t timestamp_presence_flag, uint8_t device_id_size, packet_type_t packet_type, uint8_t* buffer)
{
    buffer[1] = (uint8_t)(((protocol_version & 0x0f) << 4) | (device_id_size & 0x0f));
    buffer[0] = (uint8_t)(((packet_type & 0x0f) << 4) 
	          			| ((movement_id_presence_flag & 0x01) << 3)
			  			| ((timestamp_presence_flag & 0x01) << 2));
    return 2;	
}


int Locapack::decodeHeader(protocol_version_t* protocol_version, uint8_t* movement_id_presence_flag, 
	uint8_t* timestamp_presence_flag, uint8_t* device_id_size, packet_type_t* packet_type, uint8_t* buffer)
{
	*protocol_version = (protocol_version_t)((buffer[1] >> 4) & 0x0f);
	*device_id_size = buffer[1] & 0x0f;
	*packet_type = (packet_type_t)((buffer[0] >> 4) & 0x0f);
	*movement_id_presence_flag = (buffer[0] >> 3) & 0x01;
	*timestamp_presence_flag = (buffer[0] >> 2) & 0x01;
	return 2;
}


int Locapack::makePacket(protocol_version_t protocol_version, uint8_t movement_id_presence_flag, 
	uint8_t timestamp_presence_flag, packet_type_t packet_type, uint16_t sequence_number, 
	uint64_t timestamp, uint8_t movement_id, device_id_t* device_id, uint8_t* payload, 
	uint8_t payload_len, uint8_t* buffer)
{
	int len = 0;
	int device_id_len = 0;
	if ( device_id != NULL ) device_id_len = device_id->device_id_len;

	len += makeHeader(protocol_version, movement_id_presence_flag, timestamp_presence_flag,
	device_id_len, packet_type, buffer);
	encodeUint16(sequence_number, &buffer[len]); len += 2;
	if ( timestamp_presence_flag ) 
	{
		encodeUint40(timestamp, &buffer[len]); 
		len += 5;
	}
	if ( movement_id_presence_flag ) 
	{
		buffer[len] = movement_id; 
		len += 1;
	}
	if ( device_id_len != 0 ) 
	{
		memcpy(&buffer[len], device_id->device_id_buf, device_id->device_id_len);
		len += device_id->device_id_len;
	}
	buffer[len] = payload_len; 
	len += 1;
	if ( payload_len > 0 ) 
	{
		memcpy(&buffer[len], payload, payload_len);
		len += payload_len;
	}

	return len;
}


int Locapack::decodePacket(protocol_version_t* protocol_version, uint8_t* movement_id_presence_flag,
	uint8_t* timestamp_presence_flag, packet_type_t* packet_type, uint16_t* sequence_number, 
	uint64_t* timestamp, uint8_t* movement_id, device_id_t* device_id, uint8_t* payload, 
	uint8_t* payload_len, uint8_t* buffer)
{
	int len = 0;

	len += decodeHeader(protocol_version, movement_id_presence_flag, timestamp_presence_flag,
		&device_id->device_id_len, packet_type, buffer);
	*sequence_number = decodeUint16(&buffer[len]); len += 2;
	if ( *timestamp_presence_flag ) 
	{
		*timestamp = decodeUint40(&buffer[len]); 
		len += 5;
	}
	if ( *movement_id_presence_flag ) 
	{
		*movement_id = buffer[len];
		len += 1;
	}
	if ( device_id->device_id_len > 0 ) 
	{
		memcpy(device_id->device_id_buf, &buffer[len], device_id->device_id_len);
		len += device_id->device_id_len;
	}
	*payload_len = buffer[len];
	len += 1;
	if ( *payload_len > 0 ) 
	{
		memcpy(payload, &buffer[len], *payload_len);
		len += *payload_len;
	}
	return len;
}


int Locapack::makeUniversalGnssPayload(bool valid_gnss_position, float latitude, float longitude, bool altitude_present, float altitude, 
    bool dop_present, float dop, uint8_t* buffer)
{
	int len = 0;

	buffer[0] = (altitude_present == true ? 0x80 : 0) | (dop_present == true ? 0x40 : 0) | (valid_gnss_position == true ? 0x20 : 0);
	len += 1;

	if (valid_gnss_position)
	{
		encodeFloat(latitude, &buffer[len]); len += 4;
		encodeFloat(longitude, &buffer[len]); len += 4;
	}
	if (altitude_present) 
	{
		encodeFloat(altitude, &buffer[len]); len += 4;
	}
	if (dop_present) 
	{
		encodeFloat(dop, &buffer[len]); len += 4;
	}
	return len;
}


int Locapack::decodeUniversalGnssPayload(bool* valid_gnss_position, float* latitude, float* longitude, bool* altitude_present, float* altitude, 
    bool* dop_present, float* dop, uint8_t* buffer)
{
	int len = 0;

	if (buffer[0] & 0x80) *altitude_present = true; else *altitude_present = false;
	if (buffer[0] & 0x40) *dop_present = true; else *dop_present = false;
	if (buffer[0] & 0x20) *valid_gnss_position = true; else *valid_gnss_position = false;
	len += 1;
        if (*valid_gnss_position)
	{
		*latitude = decodeFloat(&buffer[len]); len += 4;
		*longitude = decodeFloat(&buffer[len]); len += 4;
	}
	if (*altitude_present) 
	{
		*altitude = decodeFloat(&buffer[len]); len += 4;
	}
	if (*dop_present) 
	{
		*dop = decodeFloat(&buffer[len]); len += 4;
	}
	return len;
}


int Locapack::createUniversalGnssPacket(universalGnssPacket_t* universalGnssPacket, uint8_t* buffer)
{
	int len = 0;
	uint8_t payload_buffer[MAX_BUFFER_LEN];
	int payload_buffer_len = 0;

	payload_buffer_len = makeUniversalGnssPayload(
		universalGnssPacket->valid_gnss_position,
		universalGnssPacket->latitude, 
		universalGnssPacket->longitude, 
		universalGnssPacket->altitude_present, 
		universalGnssPacket->altitude, 
		universalGnssPacket->dop_present, 
		universalGnssPacket->dop, 
		payload_buffer);

	// protocol_version_t					PROTOCOL_V1
	// uint8_t movement_id_presence_flag	false
	// uint8_t timestamp_presence_flag 		true
	// packet_type_t packet_type 			PACKET_TYPE_UNIVERSAL_GNSS
	// uint16_t sequence_number 			(internal sequence_number)
	// uint64_t timestamp					(millis)
	// uint8_t movement_id					disabled
	// device_id_t* device_id				(internal device_id)
	// uint8_t* payload						the UniversalGnssPacket generated
	// uint8_t payload_len					the generated packet length
	// uint8_t* buffer						the given buffer

	len = makePacket(PROTOCOL_V1, false, true, PACKET_TYPE_UNIVERSAL_GNSS, ++_sequence_number_universalGnssPacket, 
		(uint64_t)millis(), 0, &_device_id, payload_buffer, payload_buffer_len, buffer);

	return len;
}


int Locapack::createUniversalGnssPacketWithMovementId(universalGnssPacket_t* universalGnssPacket, uint8_t movement_id, uint8_t* buffer)
{
	int len = 0;
	uint8_t payload_buffer[MAX_BUFFER_LEN];
	int payload_buffer_len = 0;

	payload_buffer_len = makeUniversalGnssPayload(
		universalGnssPacket->valid_gnss_position,
		universalGnssPacket->latitude, 
		universalGnssPacket->longitude, 
		universalGnssPacket->altitude_present, 
		universalGnssPacket->altitude, 
		universalGnssPacket->dop_present, 
		universalGnssPacket->dop, 
		payload_buffer);

	// protocol_version_t					PROTOCOL_V1
	// uint8_t movement_id_presence_flag	false
	// uint8_t timestamp_presence_flag 		true
	// packet_type_t packet_type 			PACKET_TYPE_UNIVERSAL_GNSS
	// uint16_t sequence_number 			(internal sequence_number)
	// uint64_t timestamp					(millis)
	// uint8_t movement_id					disabled
	// device_id_t* device_id				(internal device_id)
	// uint8_t* payload						the UniversalGnssPacket generated
	// uint8_t payload_len					the generated packet length
	// uint8_t* buffer						the given buffer

	len = makePacket(PROTOCOL_V1, true, true, PACKET_TYPE_UNIVERSAL_GNSS, ++_sequence_number_universalGnssPacket, 
		(uint64_t)millis(), movement_id, &_device_id, payload_buffer, payload_buffer_len, buffer);

	return len;
}


int Locapack::makeLocallyReferencedPayload(float x, float y, bool z_present, float z, bool dop_present, float dop, 
	bool frameofref_id_present, uint64_t frameofref_id, uint8_t* buffer)
{
	int len = 0;

	buffer[0] = (z_present == true ? 0x80 : 0) | (dop_present == true ? 0x40 : 0) | (frameofref_id_present == true ? 0x20 : 0);
	len += 1;
	
	encodeFloat(x, &buffer[len]); len += 4;
	encodeFloat(y, &buffer[len]); len += 4;
	if (z_present) 
	{
		encodeFloat(z, &buffer[len]); len += 4;
	}
	if (dop_present) 
	{
		encodeFloat(dop, &buffer[len]); len += 4;
	}
	if (frameofref_id_present) 
	{
		encodeUint64(frameofref_id, &buffer[len]); len += 8;
	}
	return len;
}


int Locapack::decodeLocallyReferencedPayload(float* x, float* y, bool* z_present, float* z, bool* dop_present, float* dop, 
	bool* frameofref_id_present, uint64_t* frameofref_id, uint8_t* buffer)
{
	int len = 0;

	if (buffer[0] & 0x80) *z_present = true; else *z_present = false;
	if (buffer[0] & 0x40) *dop_present = true; else *dop_present = false;
	if (buffer[0] & 0x20) *frameofref_id_present = true; else *frameofref_id_present = false;
	
	len += 1;
	*x = decodeFloat(&buffer[len]); len += 4;
	*y = decodeFloat(&buffer[len]); len += 4;
	if (*z_present) 
	{
		*z = decodeFloat(&buffer[len]); len += 4;
	}
	if (*dop_present) 
	{
		*dop = decodeFloat(&buffer[len]); len += 4;
	}
	if (*frameofref_id_present) 
	{
		*frameofref_id = decodeUint64(&buffer[len]); len += 8;
	}
	return len;
}


int Locapack::createLocallyReferencedPacket(locallyReferencedPacket_t* locallyReferencedPacket, uint8_t* buffer)
{
	int len = 0;
	uint8_t payload_buffer[MAX_BUFFER_LEN];
	int payload_buffer_len = 0;

	payload_buffer_len = makeLocallyReferencedPayload(
		locallyReferencedPacket->x, 
		locallyReferencedPacket->y, 
		locallyReferencedPacket->z_present, 
		locallyReferencedPacket->z, 
		locallyReferencedPacket->dop_present, 
		locallyReferencedPacket->dop, 
		locallyReferencedPacket->frameofref_id_present, 
		locallyReferencedPacket->frameofref_id, 
		payload_buffer);

	// protocol_version_t					PROTOCOL_V1
	// uint8_t movement_id_presence_flag	false
	// uint8_t timestamp_presence_flag 		true
	// packet_type_t packet_type 			PACKET_TYPE_LOCALLY_REFERENCED
	// uint16_t sequence_number 			(internal sequence_number)
	// uint64_t timestamp					(millis)
	// uint8_t movement_id					disabled
	// device_id_t* device_id				(internal device_id)
	// uint8_t* payload						the LocallyReferencedPacket generated
	// uint8_t payload_len					the generated packet length
	// uint8_t* buffer						the given buffer

	len = makePacket(PROTOCOL_V1, false, true, PACKET_TYPE_LOCALLY_REFERENCED, ++_sequence_number_locallyReferencedPacket, 
		(uint64_t)millis(), 0, &_device_id, payload_buffer, payload_buffer_len, buffer);

	return len;
}


int Locapack::parseLocaPacket(locapacket_t* locapacket, uint8_t* buffer)
{
	int len = 0;
	int decoded_payload_len = 0;

	protocol_version_t protocol_version;
	uint8_t movement_id_presence_flag;
	uint8_t timestamp_presence_flag;
	packet_type_t packet_type;
	uint16_t sequence_number;
	uint64_t timestamp;
	uint8_t movement_id;
	device_id_t device_id;
	uint8_t payload[MAX_BUFFER_LEN];
	uint8_t payload_len;

	len = decodePacket(&protocol_version, &movement_id_presence_flag, &timestamp_presence_flag, &packet_type, 
		&sequence_number, &timestamp, &movement_id, &device_id, payload, &payload_len, buffer);

	if (protocol_version != PROTOCOL_V1) return 0;
	if (payload_len == 0) return 0;

	switch (packet_type)
	{
		case PACKET_TYPE_UNIVERSAL_GNSS:
			decoded_payload_len = decodeUniversalGnssPayload(&locapacket->universalGnssPacket.valid_gnss_position, 
															 &locapacket->universalGnssPacket.latitude, 
															 &locapacket->universalGnssPacket.longitude,
															 &locapacket->universalGnssPacket.altitude_present,
															 &locapacket->universalGnssPacket.altitude,
															 &locapacket->universalGnssPacket.dop_present,
															 &locapacket->universalGnssPacket.dop,
															 payload);
			if (decoded_payload_len != payload_len) return 0;
		break;

		case PACKET_TYPE_LOCALLY_REFERENCED:
			decoded_payload_len = decodeLocallyReferencedPayload(&locapacket->locallyReferencedPacket.x, 
															 	 &locapacket->locallyReferencedPacket.y,
															 	 &locapacket->locallyReferencedPacket.z_present,
															 	 &locapacket->locallyReferencedPacket.z,
															 	 &locapacket->locallyReferencedPacket.dop_present,
															 	 &locapacket->locallyReferencedPacket.dop,
															 	 &locapacket->locallyReferencedPacket.frameofref_id_present,
															 	 &locapacket->locallyReferencedPacket.frameofref_id,
															 	 payload);
			if (decoded_payload_len != payload_len) return 0;
		break;

		default:
			return 0;
	}

	return len;
}


uint16_t Locapack::decodeUint16(uint8_t *data)
{
	return 0 | (data[1] << 8) | data[0];
}


void Locapack::encodeUint16(uint16_t from, uint8_t *to)
{
	to[1] = (from & 0xFF00) >> 8;
	to[0] = from & 0xFF;
}


uint32_t Locapack::decodeUint32(uint8_t *data)
{
	return 0 | (data[3] << 24) | (data[2] << 16) | (data[1] << 8) | data[0];
}


void Locapack::encodeUint32 ( uint32_t from, uint8_t *to )
{
	to[3] = (from & 0xFF000000) >> 24;
	to[2] = (from & 0xFF0000) >> 16;
	to[1] = (from & 0xFF00) >> 8;
	to[0] = from & 0xFF;
}


uint64_t Locapack::decodeUint40 ( uint8_t *data )
{
	uint64_t tmp = 0;
	tmp = data[4];
	tmp = tmp << 32;
	tmp = tmp | decodeUint32(data);
	
	return tmp;
}


void Locapack::encodeUint40 ( uint64_t from, uint8_t *to )
{
	to[4] = (from & 0xFF00000000) >> 32;
	to[3] = (from & 0xFF000000) >> 24;
	to[2] = (from & 0xFF0000) >> 16;
	to[1] = (from & 0xFF00) >> 8;
	to[0] = from & 0xFF;
}


uint64_t Locapack::decodeUint64 ( uint8_t *data )
{
	uint64_t tmp = 0;
	tmp = decodeUint32(&data[4]); // | decodeUint32(data);
	tmp = tmp << 32;
	tmp = tmp | decodeUint32(data);
	
	return tmp;
}


void Locapack::encodeUint64 ( uint64_t from, uint8_t *to )
{
	to[7] = (from & 0xFF00000000000000) >> 56;
	to[6] = (from & 0xFF000000000000) >> 48;
	to[5] = (from & 0xFF0000000000) >> 40;
	to[4] = (from & 0xFF00000000) >> 32;
	to[3] = (from & 0xFF000000) >> 24;
	to[2] = (from & 0xFF0000) >> 16;
	to[1] = (from & 0xFF00) >> 8;
	to[0] = from & 0xFF;
}


float Locapack::decodeFloat ( uint8_t *data ) {

	typedef union _data {
		float f;
		char  s[4];
	} myData;

	myData q;

#ifndef BYTE_ORDER
    #error "BYTE_ORDER must be defined"
#endif
#if BYTE_ORDER==LITTLE_ENDIAN
	q.s[0] = data[0];
	q.s[1] = data[1];
	q.s[2] = data[2];
	q.s[3] = data[3];
#else
	q.s[0] = data[3];
    q.s[1] = data[2];
    q.s[2] = data[1];
    q.s[3] = data[0];
#endif

	return q.f;
}


void Locapack::encodeFloat ( float from, uint8_t *to ) {

	typedef union _data {
		float f;
		char  s[4];
	} myData;

	myData q;
	q.f = from;

#ifndef BYTE_ORDER
    #error "BYTE_ORDER must be defined"
#endif
#if BYTE_ORDER==LITTLE_ENDIAN
	to[0] = q.s[0];
	to[1] = q.s[1];
	to[2] = q.s[2];
	to[3] = q.s[3];
#else
    to[0] = q.s[3];
    to[1] = q.s[2];
    to[2] = q.s[1];
    to[3] = q.s[0];
#endif
}

double Locapack::decodeDouble ( uint8_t *data ) {

    typedef union _data {
        double f;
        char  s[8];
    } myData;

    myData q;
#ifndef BYTE_ORDER
    #error "BYTE_ORDER must be defined"
#endif
#if BYTE_ORDER==LITTLE_ENDIAN
    q.s[0] = data[0];
    q.s[1] = data[1];
    q.s[2] = data[2];
    q.s[3] = data[3];
    q.s[4] = data[4];
    q.s[5] = data[5];
    q.s[6] = data[6];
    q.s[7] = data[7];
#else
    q.s[0] = data[7];
    q.s[1] = data[6];
    q.s[2] = data[5];
    q.s[3] = data[4];
    q.s[4] = data[3];
    q.s[5] = data[2];
    q.s[6] = data[1];
    q.s[7] = data[0];
#endif
    return q.f;
}


void Locapack::encodeDouble ( double from, uint8_t *to ) {

    typedef union _data {
        double f;
        char  s[8];
    } myData;

    myData q;
    q.f = from;
#ifndef BYTE_ORDER
    #error "BYTE_ORDER must be defined"
#endif
#if BYTE_ORDER==LITTLE_ENDIAN
    to[0] = q.s[0];
    to[1] = q.s[1];
    to[2] = q.s[2];
    to[3] = q.s[3];
    to[4] = q.s[4];
    to[5] = q.s[5];
    to[6] = q.s[6];
    to[7] = q.s[7];
#else
    to[0] = q.s[7];
    to[1] = q.s[6];
    to[2] = q.s[5];
    to[3] = q.s[4];
    to[4] = q.s[3];
    to[5] = q.s[2];
    to[6] = q.s[1];
    to[7] = q.s[0];
#endif
}
