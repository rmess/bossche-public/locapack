// Locapack.h
// 
// Locapack is an Arduino library that implements the localisation packet protocol from the IRIT/RMESS team. 
//  
/// \mainpage Locapack library for Arduino
/// 
/// @author Adrien van den Bossche <vandenbo@univ-tlse2.fr>

#ifndef Locapack_h
#define Locapack_h

#include "Arduino.h"

class Locapack {

	public:

		#define MAX_DEVICE_ID_LEN 8
		#define MAX_BUFFER_LEN 128

        ///
        /// @brief
        ///
		typedef enum {
			PROTOCOL_V1 = 1
		} protocol_version_t;

        ///
        /// @brief
        ///
		typedef enum {
			PACKET_TYPE_UNIVERSAL_GNSS = 1,
			PACKET_TYPE_LOCALLY_REFERENCED = 2
		} packet_type_t;

        ///
        /// @brief
        ///
		typedef struct {
			uint8_t device_id_buf[MAX_DEVICE_ID_LEN];
			uint8_t device_id_len;
		} device_id_t;

        ///
        /// @brief
        ///
		typedef struct {
			bool valid_gnss_position;
			float latitude;
			float longitude;
			bool altitude_present;
			float altitude;
			bool dop_present;
			float dop;
		} universalGnssPacket_t;

        ///
        /// @brief
        ///
		typedef struct {
			float x;
			float y;
			bool z_present;
			float z;
			bool dop_present;
			float dop;
			bool frameofref_id_present;
			uint64_t frameofref_id;
		} locallyReferencedPacket_t;

        ///
        /// @brief
        ///
		typedef union locapacket {
        	universalGnssPacket_t universalGnssPacket;
        	locallyReferencedPacket_t locallyReferencedPacket;
    	} locapacket_t;

        ///
        /// @brief
        ///
		device_id_t _device_id;

        ///
        /// @brief
        ///
		uint16_t _sequence_number_universalGnssPacket;

        ///
        /// @brief
        ///
		uint16_t _sequence_number_locallyReferencedPacket;

		///
		/// @brief Locapack Constructor
		///
		Locapack();

		///
		/// @brief 
		/// @param 
		///
		void setdevice_id16(uint16_t sourceAddress);

		///
		/// @brief 
		/// @param 
		///
		void setdevice_id32(uint32_t sourceAddress);

		///
		/// @brief 
		/// @param 
		///
		void setdevice_id64(uint64_t sourceAddress);

		///
		/// @brief 
		/// @param 
		///
		int makeHeader(protocol_version_t protocol_version,	uint8_t movement_id_presence_flag, 
			uint8_t timestamp_presence_flag, uint8_t device_id_size, packet_type_t packet_type, uint8_t* buffer);

		///
		/// @brief 
		/// @param 
		///
		int decodeHeader(protocol_version_t* protocol_version, uint8_t* movement_id_presence_flag, 
			uint8_t* timestamp_presence_flag, uint8_t* device_id_size, packet_type_t* packet_type, uint8_t* buffer);

		///
		/// @brief 
		/// @param 
		///
		int makePacket(protocol_version_t protocol_version,	uint8_t movement_id_presence_flag, 
			uint8_t timestamp_presence_flag, packet_type_t packet_type, uint16_t sequence_number, 
			uint64_t timestamp, uint8_t movement_id, device_id_t* device_id, uint8_t* payload, 
			uint8_t payload_len, uint8_t* buffer);

		///
		/// @brief 
		/// @param 
		///
		int decodePacket(protocol_version_t* protocol_version, uint8_t* movement_id_presence_flag,
			uint8_t* timestamp_presence_flag, packet_type_t* packet_type, uint16_t* sequence_number, 
			uint64_t* timestamp, uint8_t* movement_id, device_id_t* device_id, uint8_t* payload, 
			uint8_t* payload_len, uint8_t* buffer);		

		///
		/// @brief 
		/// @param 
		///
		int makeUniversalGnssPayload(bool valid_gnss_position, float latitude, float longitude, bool altitude_present, float altitude, 
			bool dop_present, float dop, uint8_t* buffer);

		///
		/// @brief 
		/// @param 
		///
		int decodeUniversalGnssPayload(bool* valid_gnss_position, float* latitude, float* longitude, bool* altitude_present, float* altitude, 
			bool* dop_present, float* dop, uint8_t* buffer);

		///
		/// @brief 
		/// @param 
		///
		int createUniversalGnssPacket(universalGnssPacket_t* gnss, uint8_t* buffer);

		///
		/// @brief 
		/// @param 
		///
        int createUniversalGnssPacketWithMovementId(universalGnssPacket_t* universalGnssPacket, uint8_t movement_id, uint8_t* buffer);

		///
		/// @brief 
		/// @param 
		///
		int makeLocallyReferencedPayload(float x, float y, bool z_present, float z, bool dop_present, float dop, 
			bool frameofref_id_present, uint64_t frameofref_id, uint8_t* buffer);
		///
		/// @brief 
		/// @param 
		///
		int decodeLocallyReferencedPayload(float* x, float* y, bool* z_present, float* z, bool* dop_present, float* dop, 
			bool* frameofref_id_present, uint64_t* frameofref_id, uint8_t* buffer);
		///
		/// @brief 
		/// @param 
		///
		int createLocallyReferencedPacket(locallyReferencedPacket_t* LocallyReferencedPacket, uint8_t* buffer);

		///
		/// @brief 
		/// @param 
		///
		int parseLocaPacket(locapacket_t* locapacket, uint8_t* buffer);

	private:
	
		///
		/// @brief Builds an uint16 value from two uint8 values
		/// @param data The address of the uint8_t buffer
		/// @return The decoded uint16_t
		/// 
		uint16_t decodeUint16 ( uint8_t *data );

		///
		/// @brief Formats an uint16 value as a list of uint8 values
		/// @param from The uint16_t value
		/// @param to The address of the uint8_t buffer
		/// 
		void encodeUint16 ( uint16_t from, uint8_t *to );

		///
		/// @brief Builds an uint32 value from four uint8 values
		/// @param data The address of the uint8_t buffer
		/// @return The decoded uint32_t
		/// 
		uint32_t decodeUint32 ( uint8_t *data );

		///
		/// @brief Formats an uint32 value as a list of uint8 values
		/// @param from The uint32_t value
		/// @param to The address of the uint8_t buffer
		/// 
		void encodeUint32 ( uint32_t from, uint8_t *to );

		///
		/// @brief Builds an uint64 value from five uint8 values
		/// @param data The address of the uint8_t buffer
		/// @return The decoded uint64_t
		/// 
		uint64_t decodeUint40 ( uint8_t *data );

		///
		/// @brief Formats an uint64 value with only 5 LSbytes as a list of uint8 values
		/// @param from The uint64_t value
		/// @param to The address of the uint8_t buffer
		/// 
		void encodeUint40 ( uint64_t from, uint8_t *to );

		///
		/// @brief Builds an uint64 value from eight uint8 values
		/// @param data The address of the uint8_t buffer
		/// @return The decoded uint64_t
		/// 
		uint64_t decodeUint64 ( uint8_t *data );

		///
		/// @brief Formats an uint64 value as a list of uint8 values
		/// @param from The uint64_t value
		/// @param to The address of the uint8_t buffer
		/// 
		void encodeUint64 ( uint64_t from, uint8_t *to );
		
		///
		/// @brief Builds a float value from four uint8 values
		/// @param data The address of the uint8_t buffer
		/// @return The decoded float
		///
		float decodeFloat ( uint8_t *data );

		///
		/// @brief Formats an float value as a list of uint8 values
		/// @param from The float value
		/// @param to The address of the uint8_t buffer
		/// @return No return
		/// 
		void encodeFloat ( float from, uint8_t *to );	

        ///
        /// @brief Builds a double value from four uint8 values
        /// @param data The address of the uint8_t buffer
        /// @return The decoded double
        ///
        double decodeDouble ( uint8_t *data );

        ///
        /// @brief Formats an float value as a list of uint8 values
        /// @param from The float value
        /// @param to The address of the uint8_t buffer
        /// @return No return
        ///
        void encodeDouble ( double from, uint8_t *to );

	protected:

};

#endif
