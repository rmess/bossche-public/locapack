#include <Locapack.h>
#include <TinyGPS++.h>

#define LOCAPACK_PACKET_PERIOD 5000

Locapack locapack;
TinyGPSPlus gps;
uint8_t buffer[128];
Locapack::device_id_t device_id;
Locapack::universalGnssPacket_t gnss;
Locapack::locapacket_t locapacket;
bool gnss_valid = false;
uint32_t gnss_age = 0;
uint32_t timetosend = LOCAPACK_PACKET_PERIOD;

void setup()
{
  // Set console and GPS serial baudrate
  Serial.begin(115200);
  Serial1.begin(9600);

  locapack.setdevice_id16(0x1234);
  gnss.altitude_present = true;
  gnss.dop_present = true;
}

void loop()
{
  if (millis() > timetosend)
  {
    timetosend += LOCAPACK_PACKET_PERIOD;
    
    if (gnss_valid)
    {
      Serial.print("I create a new packet with position recorded ");
      Serial.print(millis()-gnss_age);
      Serial.println("ms ago.");

      int len = locapack.createUniversalGnssPacket(&gnss, buffer);
      for (int i=0; i<len; i++)
      {
        Serial.print(buffer[i], HEX);
        Serial.print(" ");
      }
      Serial.println();

      Serial.println("Now decode this packet...");
      len = locapack.parseLocaPacket(&locapacket, buffer);
      Serial.print("packet len="); Serial.println(len);
      Serial.print("latitude="); Serial.println(locapacket.universalGnssPacket.latitude);
      Serial.print("longitude="); Serial.println(locapacket.universalGnssPacket.longitude);
      Serial.print("altitude="); Serial.println(locapacket.universalGnssPacket.altitude);
      Serial.print("hdop="); Serial.println(locapacket.universalGnssPacket.dop);
    }
    else
    {
      Serial.println("\n\n*** Waiting for a valid position ***\n\n");
    }
  }

  // Get chars from GPS receiver
  while (Serial1.available())
  {
    char c = Serial1.read();
    //Serial.write(c); // uncomment this line if you want to see the GPS data flowing

    if (gps.encode(c))
    {
      // Did a new valid sentence come in?
      if ( gps.location.isValid() ) 
      {
        gnss.latitude = gps.location.lat();
        gnss.longitude = gps.location.lng();
        gnss.altitude = gps.altitude.meters();
        gnss.dop = gps.hdop.hdop();
        gnss_age = millis();
        gnss_valid = true;
      }
    }
  }
}
