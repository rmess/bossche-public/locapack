#include <Locapack.h>

#define LOCAPACK_PACKET_PERIOD 2000

Locapack locapack;
uint8_t buffer[128];
Locapack::device_id_t device_id;
Locapack::universalGnssPacket_t gnss;
Locapack::locapacket_t locapacket;
uint32_t timetosend = LOCAPACK_PACKET_PERIOD;

void setup()
{
  Serial.begin(115200);
  locapack.setdevice_id16(0x1234);

  gnss.altitude_present = true;
  gnss.dop_present = true;
}

void loop()
{
  gnss.latitude = 1.0;
  gnss.longitude = 2.0;
  gnss.altitude = 3.0;
  gnss.dop = 4.0;

  // Universal GNSS Packet creation
  Serial.print("I create a new packet! len=");
  int len = locapack.createUniversalGnssPacket(&gnss, buffer);
  Serial.print(len);
  Serial.println("byte(s): ");
  for (int i=0; i<len; i++)
  {
    Serial.print(buffer[i], HEX);
    Serial.print(" ");
  }
  Serial.println();

  // Universal GNSS Packet decoding
  Serial.println("Now try to decode this packet...");
  len = locapack.parseLocaPacket(&locapacket, buffer);
  Serial.print("packet len="); Serial.println(len);
  Serial.print("latitude="); Serial.println(locapacket.universalGnssPacket.latitude);
  Serial.print("longitude="); Serial.println(locapacket.universalGnssPacket.longitude);
  Serial.print("altitude="); Serial.println(locapacket.universalGnssPacket.altitude);
  Serial.print("hdop="); Serial.println(locapacket.universalGnssPacket.dop);

  // Delay
  delay(LOCAPACK_PACKET_PERIOD);
}
