#include <Locapack.h>

#define LOCAPACK_PACKET_PERIOD 2000

Locapack locapack;
uint8_t buffer[128];
Locapack::device_id_t device_id;
Locapack::locallyReferencedPacket_t locRef;
Locapack::locapacket_t locapacket;

void printUint64 ( uint64_t ui64 )
{
  uint8_t buf[8];

  buf[7] = (ui64 & 0xFF00000000000000) >> 56;
  buf[6] = (ui64 & 0xFF000000000000) >> 48;
  buf[5] = (ui64 & 0xFF0000000000) >> 40;
  buf[4] = (ui64 & 0xFF00000000) >> 32;
  buf[3] = (ui64 & 0xFF000000) >> 24;
  buf[2] = (ui64 & 0xFF0000) >> 16;
  buf[1] = (ui64 & 0xFF00) >> 8;
  buf[0] = ui64 & 0xFF;

  for (int i=7; i>=0; i--)
  {
    if (buf[i]<0x10) Serial.print("0"); 
    Serial.print(buf[i], HEX); 
    Serial.print(" ");
  }
}

void setup()
{
  Serial.begin(115200);
  locapack.setdevice_id16(0x1234);

  locRef.z_present = true;
  locRef.dop_present = true;
  locRef.frameofref_id_present = true;
}

void loop()
{
  locRef.x = 1.0;
  locRef.y = 2.0;
  locRef.z = 3.0;
  locRef.dop = 4.0;
  locRef.frameofref_id = 0x1122334455667788;

  // Locally Referenced Packet creation
  Serial.print("I create a new packet! len=");
  int len = locapack.createLocallyReferencedPacket(&locRef, buffer);
  Serial.print(len);
  Serial.println("byte(s): ");
  for (int i=0; i<len; i++)
  {
    Serial.print(buffer[i], HEX);
    Serial.print(" ");
  }
  Serial.println();

  // Locally Referenced Packet decoding
  Serial.println("Now try to decode this packet...");
  len = locapack.parseLocaPacket(&locapacket, buffer);
  Serial.print("packet len="); Serial.println(len);
  Serial.print("x="); Serial.println(locapacket.locallyReferencedPacket.x);
  Serial.print("y="); Serial.println(locapacket.locallyReferencedPacket.y);
  Serial.print("z="); Serial.println(locapacket.locallyReferencedPacket.z);
  Serial.print("hdop="); Serial.println(locapacket.locallyReferencedPacket.dop);
  Serial.print("frameofref_id="); printUint64(locapacket.locallyReferencedPacket.frameofref_id);
  Serial.println();

  // Delay
  delay(LOCAPACK_PACKET_PERIOD);
}
