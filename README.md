# locapack

## Brief

Another localisation packet protocol to send/receive localisation data over networks.

## Details

LocaPack is an implementation of the locapack protocol used to send or receive various localisation data over wired and wireless networks.

LocaPack enables the transport of:
- universal GNSS localisation coordonates such as latitude/longitude data, 
- locally referenced localisations, such as {x,y} positions in an indoor environment,
- inter-node proximity information such as ranging data.

Some extra features are also avalable, such as:
- automatic mapping/translation of coordonates between universal GNSS/locally referenced localisations,
- movement_id, that enables extra movement information based on another sensor (typically a local accelerometer),
- packet sequence numbering and local timestamping, to track positions and detect missing information,
- message hashing, to send shorter but incomplete payloads, to respect privacy.
 
Several implementations are available: 
- Embedded: C++ class (Arduino library)
- Infrastrucure/IoT: Node-RED palette
- Portable: python

A Json description of the messages is also available.

## Implementation

Implementation status:

- Arduino
  - encode/decode header,
  - encode/decode universal GNSS packets (type 1) with example/test sketch,
  - encode/decode locally referenced packets (type 2) with example/test sketch,
  - example sketch with GPS receiver.
- Node-RED
  - encode/decode header,
  - encode/decode universal GNSS packets,
  - encode/decode locally referenced packets.

### Arduino

LocaPack is available as an Arduino library, to produce or decode locapack messages.

#### Installation

1. Clone the LocaPack project,
1. Copy the `Arduino/LocaPack` directory into the libraries directory (typically `~/Arduino/libraries`)
1. Launch Arduino IDE and start with one of the examples.

### Node-RED

LocaPack is available as a Node-RED Palette, ie a collection of nodes to produce or decode locapack messages.

![Drag Racing](nodered/node-red-flow-example.png)

#### Installation

##### Install via Node-RED palette

Locapack is available on npm: `node-red-contrib-locapack`

In the Node-RED interface, click on top-right button > Manage palette, than search for `node-red-contrib-locapack`

##### Local installation (developpers)

1. Clone the LocaPack project,
1. In your Node-RED user directory, typically ~/.node-red, run: `npm install <location of node module>` where location is the path to the `nodered` folder of the git cloned repository.
1. Restart Node-RED.

### JSON messages

LocaPack messages are also available in JSON. 

Example of a Universal GNSS localisation packet:
```json
{
  "header": {
    "protocol_version": 1,
    "device_id_size": 2,
    "packet_type": 1,
    "movement_id_presence_flag": false,
    "timestamp_presence_flag": true
  },
  "sequence_number": 2746,
  "timestamp": 27850001,
  "device_id": 237,
  "payload": [192,49,131,47,66,150,8,165,63,154,153,18,67,164,112,157,63],
  "universalGnssPacket": {
    "header": {
      "altitude_presence_flag": true,
      "dop_presence_flag": true
    },
    "latitude": 43.878116607666016,
    "longitude": 1.2893245220184326,
    "altitude": 146.60000610351562,
    "dop": 1.2300000190734863
  }
}
```

Example of a Locally Referenced localisation packet:
```json
{
  "LocaPack": {
    "header": {
      "protocol_version": 1,
      "device_id_size": 2,
      "packet_type": 2,
      "movement_id_presence_flag": false,
      "timestamp_presence_flag": true
    },
    "sequence_number": 163,
    "timestamp": 1630289,
    "device_id": 124,
    "payload": [224,0,0,128,63,0,0,0,64,0,0,64,64,0,0,128,64,136,119,102,85,68,51,34,17],
    "locallyReferencedPacket": {
      "header": {
        "z_presence_flag": true,
        "dop_presence_flag": true,
        "frameofref_id_presence_flag": true
      },
      "x": 1,
      "y": 2,
      "z": 3,
      "dop": 4,
      "frameofref_id": 9833440827789222000
    }
  }
}
```

## License

Locapack is distributed under the GPLv3 license. Please refer to the `LICENSE` file.


## Packet structure details

Message endianness is little endian.

### Packet general structure

|    2B    |         2B        |    0/5B     |    1B (opt)   |  variable   |      1B       |            variable           |
|:--------:|:-----------------:|:-----------:|:-------------:|:-----------:|:-------------:|:-----------------------------:|
| `header` | `sequence_number` | `timestamp` | `movement_id` | `device_id` | `payload_len` | `payload` |

The `sequence_number` field is mandatory.

The `timestamp` field is optional and its presence is announced in the header. The timestamp field contains the number of milliseconds since the boot of the local system. There is no synchronisation between different nodes. 

The `movement_id` field is optional and its presence is announced in the header.

The `device_id` field length is annouced in the header. A length of 0 means that the field is not present in the packet, for example if the `device_id` is the same as the source address of the packet.

The `payload_len` field contains the length of the payload.

The `payload` length depends on the `packet_type` annouced in the header.


### Header structure

| MSB     4b         |        4b        |      4b       |             1b              |           1b              |  2b LSB  |
|--------------------|------------------|---------------|-----------------------------|---------------------------|----------|
| `protocol_version` | `device_id_size` | `packet_type` | `movement_id_presence_flag` | `timestamp_presence_flag` | reserved |

The `protocol_version` field indicates the version of the localisation packet used. This documentation describes the version #1 of the localisation packet protocol.

The `device_id_size` indicates the number of Bytes of the `device_id` field. A length of 0 means that no `device_id` is present in the packet.

The `packet_type` indicates the structure of the payload. These types of packet have been described:

  * `type 1` : Universal GNSS localisation
  * `type 2` : Locally referenced localisation
  * `type 3` : Inter-node proximity

The `movement_id_presence_flag` must be set to 1 if the `movement_id` field is present in the packet.

The `timestamp_presence_flag` must be set to 1 if the `timestamp` field is present in the packet.

### Payload structure by packet type

#### Type 1 : Universal GNSS localisation

Transport of GNSS coordinates. Altitude and DOP are optionnal. 

##### Payload general structure

|    1B    |     4B      |     4B      |    0/4B    | 0/4B  |
|----------|-------------|-------------|------------|-------|
| `header` | `latitude`  | `longitude` | `altitude` | `dop` |

The `latitude` field is a C-type float and is mandatory if the `valid_gnss_position` flag is set

The `longitude` field is a C-type float and is mandatory if the `valid_gnss_position` flag is set

The `altitude` field is a C-type float and its presence is annouced in the `header`

The `dop` field is a C-type float and its presence is annouced in the `header`

##### Header

|           1b             |         1b          |           1b          |    6b    |
|--------------------------|---------------------|-----------------------|----------|
| `altitude_presence_flag` | `dop_presence_flag` | `valid_gnss_position` | reserved |

The `altitude_presence_flag` must be set to 1 if the `altitude` field is present in the packet.

The `dop_presence_flag` must be set to 1 if the `dop` field is present in the packet.

The `valid_gnss_position` must be set to 1 if the `latitude` and `longitude` fields are present in the packet.

#### Type 2 : Locally referenced localisation

Transport of locally referenced 2D or 3D localisation. The "repère" is identified by an UID. 

##### Payload general structure

|    1B    | 4B  | 4B  | 0/4B | 0/4B  |      0/8B       |
|----------|-----|-----|------|-------|-----------------|
| `header` | `x` | `y` |  `z` | `dop` | `frameofref_id` |

The `x` field is a C-type float and is mandatory.

The `y` field is a C-type float and is mandatory

The `z` field is a C-type float and its presence is annouced in the `header`

The `dop` field is a C-type float and its presence is annouced in the `header`

The `frameofref_id` field is a 64-bit UID that identify the Frame of Reference ("le repère utilisé") and its presence is annouced in the `header`
    
##### Header

|        1b         |         1b          |               1b               |    5b    |
|-------------------|---------------------|--------------------------------|----------|
| `z_presence_flag` | `dop_presence_flag` |  `frameofref_id_presence_flag` | reserved |

The `z_presence_flag` must be set to 1 if the `z` field is present in the packet (3D localisation).

The `dop_presence_flag` must be set to 1 if the `dop` field is present in the packet.

The `frameofref_id_presence_flag` must be set to 1 if the `frameofref_id` field is present in the packet.

#### Type 3 : Inter-node proximity

Transport of raw distances (rangings) between pairs of nodes. The ranging technology can be specified. 

